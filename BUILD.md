podman build -t mssql:latest .
podman stop mssql
podman rm mssql

podman run -e "ACCEPT_EULA=Y" -e "MSSQL_SA_PASSWORD=yourStrong(#)Password" --name mssql -p 1433:1433 -it mssql:latest /bin/bash
podman run -e "ACCEPT_EULA=Y" -e "MSSQL_SA_PASSWORD=yourStrong(#)Password" --name mssql -p 1433:1433 -d mssql:latest
podman logs -f mssql

podman pull registry.gitlab.com/imosag/helm-ix-net/mssql:latest


podman run -u mssql -e "ACCEPT_EULA=Y" -e "MSSQL_SA_PASSWORD=yourStrong(#)Password" -p 1433:1433 --name mssql -it mcr.microsoft.com/mssql/rhel/server:2022-preview-rhel-9 /bin/bash


sqlcmd -C -U sa -P "$MSSQL_SA_PASSWORD" 

or

sqlcmd -C -b -U ${MSSQL_USER} -P ${MSSQL_SA_PASSWORD} -d ${MSSQL_DATABASE}
